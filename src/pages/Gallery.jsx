import React, {useEffect, useState} from 'react'
import Fab from '../components/Fab'
import Thumbnail from '../components/Thumbnail'
import {Link} from 'react-router-dom'

// const images = [{
//     id: "1000",
//     author: "0",
//     url: "https://picsum.photos/id/1000/4312/2868",
//     description: "Sample from Unsplash"
// },
// {
//     id: "1001",
//     author: "0",
//     url: "https://picsum.photos/id/1001/4312/2868",
//     description: "Sample from Unsplash"
// },
// {
//     id: "1002",
//     author: "0",
//     url: "https://picsum.photos/id/1002/4312/2868",
//     description: "Sample from Unsplash"
// }]

const Gallery = () => {

    const [images, setImages] = useState([]);

    const fetchImages = async () => fetch("http://localhost:8080/api/images")
        .then((res) => res.json())
        .then((res) => setImages(res))
        .catch((error) => console.error(error));
    
    useEffect(() => {
        fetchImages()
    }, []);

    return <div className="mx-auto">
        <ul className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-6 gap-1 mx-auto">
            {
                images.map((image)=>{
                    return  (<li key={image.id}>
                        <Thumbnail url={image.url} description={image.description}/>
                    </li>)
                })

            }
            
        </ul>
        <Link to="/gallery/upload"><Fab icon="+" /></Link>
    </div>
}

export default Gallery