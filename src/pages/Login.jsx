import React, {useState , useReducer} from 'react'
import LoginForm from '../components/LoginForm'
import Alert from '../components/Alert'
import { json } from 'react-router-dom'

const Login = () => {
  
  // TEST REDUCER
    const initialArg = {count:0}

    const reducer = (state, action) => {
      switch(action.type){
          case 'increment':
              return {
                  count: state.count + action.payload
              }
          case 'decrement':
              return {
                  count: state.count-1
              }
          case 'reset':
              return {
                  count: 0
              }
          default:
              return state
    }
}
   // const [state , dispatch] = useReducer(reducer , initialArg)


  //FIN TEST REDUCER

  //etat initial de alertData
  const [alertData, setAlertData] = useState(null);

  const [authInit , setAuth] = useState(
    {
      loading: false,
      error: false,
      user: null
    }
  )

  const authReducer = (state, action)=>{
    switch(action.type){
      case 'login_start':
        console.log('start');
          return {
              authInit : {
                loading: true,
                error: false
              }
          }
      case 'login_error':
        console.log('error..');
          return {
              authInit : {
                loading: false,
                error: true
              }
          }
      case 'login_success':
        console.log('success !!! ');
          return {
            authInit : {
              loading: false,
              error: false,
              user: action.payload
            }
          }
      default:
          return state
  }
}
const [state , dispatch] = useReducer(authReducer , authInit)



  const onSubmitLoginForm = (credentials) => {
    console.log(credentials)
    dispatch({type: 'login_start'})

    // on envoie au serveur nos credentials
    fetch('http://localhost:8080/api/auth/login ', {
      method: 'POST',
      body: JSON.stringify(credentials),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        // setSubmitResponse(res);
        return res.json();
      })
      .then((res) => {

        console.log("result", res);


        if(res.error) {
          dispatch({type: 'login_error'})

          setAlertData({
            status: "error",
            description: res.error
          })

        
        } else if (res.msg){
          dispatch({type: 'login_success' ,  payload:res })

            setAlertData({
              status: "success",
              description: res.msg
            })
        }

      })
      /*.then((res) => {


        console.log("result", res);

        
        if(res.error) {// si on a l'attribut error dans les résultats
          // c'est une erreur
          setAlertData({
            status: "error",
            description: res.error
          })
        
        } else if (res.msg) // sinon si on a l'attribut msg dans les résultat
          // c'est un succés
          setAlertData({
            status: "success",
            description: res.msg
          })
      })*/
  }

  return (
    <div className="container mx-auto px-4 h-full">
      <div className="flex content-center items-center justify-center h-full">
        <div className="w-full lg:w-4/12 px-4">
          {/* Mettez l'alerte ici ! */}
          {alertData && (<Alert status={alertData.status} description={alertData.description}/> )}
          <LoginForm
            onSubmit={onSubmitLoginForm}
            disabled={false}
          />
        </div>
      </div>
      <div>
          <p>Count : {state.count}</p>
          <button onClick={() => dispatch({type: 'increment',    payload: 1 })}>
              Increment
          </button>
          <button onClick={() => dispatch('decrement')}>
              Decrement
          </button>
          <button onClick={() => dispatch('reset')}>
              Reset
          </button>
        </div>
    </div>
  )
}

export default Login