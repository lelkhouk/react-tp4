import React from 'react'

import Home from '../pages/Home'
import Login from '../pages/Login'
import Gallery from '../pages/Gallery'
import Layout from './Layout'
import Logout from '../pages/Logout'
import Upload from '../pages/Upload'
import NotFound from './NotFound'
import {Routes,BrowserRouter, Route } from 'react-router-dom'

const App = () => {

    return <BrowserRouter>
            <Layout>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/logout" element={<Logout />} />
                    <Route path="/gallery" element={<Gallery />} />
                    <Route path="/gallery/upload" element={<Upload />} />
                    <Route path="*" element={<NotFound/>} />
                </Routes>
            </Layout>
        </BrowserRouter>
}

export default App